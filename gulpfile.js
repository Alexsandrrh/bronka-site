const gulp = require("gulp"),
  watch = require("gulp-watch"), // нужен для наблюдения за изменениями файлов (со встроенным были проблемы)
  prefixer = require("gulp-autoprefixer"), // автоматически добавляет вендорные префиксы к CSS свойствам
  terser = require("gulp-terser"), // будет сжимать наш JS
  cssnano = require("gulp-cssnano"), // нужен для сжатия CSS кода
  jade = require("gulp-jade"), // для компиляции нашего JADE кода (можно использовать HTML и собирать rigger'ом)
  sass = require("gulp-sass"), // для компиляции нашего SASS кода (можно использовать LESS)
  sourcemaps = require("gulp-sourcemaps"), // возьмем для генерации css sourscemaps, которые будут помогать нам при отладке кода
  rigger = require("gulp-rigger"), // позволяет импортировать один файл в другой простой конструкцией (//= footer.html)
  imagemin = require("gulp-imagemin"), // для сжатия картинок
  spritesmith = require("gulp.spritesmith"), // для создания спрайта
  urlAdjuster = require("gulp-css-url-adjuster"), // смена адреса
  pngquant = require("imagemin-pngquant"), // дополнения к gulp-imagemin, для работы с PNG
  rimraf = require("rimraf"), // rm -rf для ноды
  browserSync = require("browser-sync"), // с помощью этого плагина мы можем легко развернуть локальный dev сервер с блэкджеком и livereload
  reload = browserSync.reload,
  runSequence = require("run-sequence"),
  concat = require("gulp-concat"),
  If = require("gulp-if"),
  { argv } = require("yargs"),
  // svg sprites
  svgSprite = require("gulp-svg-sprite"),
  svgmin = require("gulp-svgmin"),
  cheerio = require("gulp-cheerio"),
  replace = require("gulp-replace"),
  // svg sprites:end
  debug = require("gulp-debug");

const isProd = argv.production;
const isDev = argv.development;

const path = {
  build: {
    //Тут мы укажем куда складывать готовые после сборки файлы
    html: "static/",
    js: "static/js/",
    css: "static/css/",
    img: "static/img/",
    svg: "static/svg/",
    fonts: "static/fonts/"
  },
  src: {
    //Пути откуда брать исходники
    jade: "src/*.jade", //Синтаксис src/*.jade говорит gulp что мы хотим взять все файлы с расширением .jade
    js: ["src/js/main.js"], //В стилях и скриптах нам понадобятся только main файлы
    style: "src/style/main.scss",
    img: "src/img/**/*.*",
    svg: "src/svg/**/*.svg",
    sprite: "src/sprite/**/*.*",
    spriteRetina: "src/sprite/",
    scssSprite: "src/style/partials/",
    fonts: "src/fonts/**/*.*"
  },
  watch: {
    //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
    jade: "src/**/*.jade",
    js: ["src/js/**/*.js"],
    style: "src/style/**/*.scss",
    sprite: "src/sprite/**/*.*",
    img: "src/img/**/*.*",
    svg: "src/svg/**/*.svg",
    fonts: "src/fonts/**/*.*"
  },
  clean: "./static"
};

const config = {
  server: {
    baseDir: "./static"
  },
  open: false,
  tunnel: false,
  host: "localhost",
  port: 9008,
  logPrefix: "frontend_dev",
  notify: false
};

gulp.task("html:build", function() {
  gulp
    .src(path.src.jade) //Выберем файлы по нужному пути
    .pipe(
      jade({
        pretty: "   "
      })
    )
    .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
    .pipe(reload({ stream: true })); //И перезагрузим наш сервер для обновлений
});

gulp.task("js:build", function() {
  runSequence("js:main");
});

gulp.task("js:main", function() {
  return gulp
    .src(path.src.js) // Найдем наш main файл и клиентские файлы компонентов
    .pipe(sourcemaps.init())
    .pipe(rigger()) // Прогоним через rigger
    .pipe(concat("main.js"))
    // .pipe(If(isProd, terser()))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(path.build.js)) // Выплюнем готовый файл в build
    .pipe(reload({ stream: true })); // И перезагрузим сервер
});

gulp.task("style:build", function() {
  runSequence("style:main");
});

gulp.task("style:main", function() {
  return gulp
    .src(path.src.style) // Выберем наш main.scss
    .pipe(sourcemaps.init())
    .pipe(sass()) // Скомпилируем
    .pipe(
      prefixer({
        browsers: ["ie 8", "ie 9", "Firefox 5.0", "Opera 12.0", "Safari 4"]
      })
    ) // Добавим вендорные префиксы
    .pipe(
      urlAdjuster({
        replace: ["../../", "../"]
      })
    )
    // .pipe(If(isProd, cssnano()))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(path.build.css)) // И в build
    .pipe(reload({ stream: true }));
});

// создание спрайта
gulp.task("sprite:build", function() {
  const spriteData = gulp.src(path.src.sprite).pipe(
    spritesmith({
      imgName: "sprite.png",
      cssName: "_sprite.scss",
      cssFormat: "scss",
      algorithm: "top-down", // binary-tree - плохой алгоритм для ретины
      imgPath: "../img/sprite.png",
      padding: 5,
      cssVarMap: function(sprite) {
        sprite.name = "ic-" + sprite.name;
      }
    })
  );

  spriteData.img.pipe(gulp.dest(path.build.img)); // путь, куда сохраняем картинку
  spriteData.css.pipe(gulp.dest(path.src.scssSprite)); // путь, куда сохраняем стили
  spriteData.pipe(reload({ stream: true }));
});

// копирование и сжатие картинок
gulp.task("image:build", function() {
  runSequence("image:project");
});

gulp.task("image:project", function() {
  return gulp
    .src(path.src.img) //Выберем наши картинки
    .pipe(
      imagemin({
        // Сожмем их
        progressive: true,
        // svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()],
        interlaced: true
      })
    )
    .pipe(gulp.dest(path.build.img)) // И бросим в build
    .pipe(reload({ stream: true }));
});

// svg
gulp.task("svg:build", function() {
  return gulp
    .src(path.src.svg) //Выберем наши картинки
    .pipe(gulp.dest(path.build.svg)) // И бросим в build
    .pipe(reload({ stream: true }));
});

gulp.task("svgSpriteBuild", function() {
  return (
    gulp
      .src("src/img/icons/*.svg")
      // minify svg
      .pipe(
        svgmin({
          js2svg: {
            pretty: true
          }
        })
      )
      // remove all fill, style and stroke declarations in out shapes
      .pipe(
        cheerio({
          run: function($) {
            $("[fill]").removeAttr("fill");
            $("[stroke]").removeAttr("stroke");
            $("[style]").removeAttr("style");
          },
          parserOptions: { xmlMode: true }
        })
      )
      // cheerio plugin create unnecessary string '&gt;', so replace it.
      .pipe(replace("&gt;", ">"))
      // build svg sprite
      .pipe(
        svgSprite({
          mode: {
            symbol: {
              sprite: "../sprite.svg",
              render: {
                scss: {
                  dest: "../../style/helpers/_sprite.scss",
                  template: "src/style/templates/_sprite_template.scss"
                }
              }
            }
          }
        })
      )
      .pipe(gulp.dest("src/svg/"))
  );
});

gulp.task("svgSprite", ["svgSpriteBuild"]);

// копирование шрифтов
gulp.task("fonts:build", function() {
  gulp.src(path.src.fonts).pipe(gulp.dest(path.build.fonts));
});

gulp.task("build", [
  "html:build",
  "js:build",
  "sprite:build",
  "style:build",
  "fonts:build",
  // 'image:build',
  "svg:build"
]);

gulp.task("watch", ["build"], function() {
  watch([path.watch.jade], function(event, cb) {
    gulp.start("html:build");
  });
  watch([path.watch.style], function(event, cb) {
    gulp.start("style:main");
  });
  watch(path.watch.js, function(event, cb) {
    gulp.start("js:main");
  });
  watch([path.watch.sprite], function(event, cb) {
    gulp.start("sprite:build");
  });
  watch([path.watch.img], function(event, cb) {
    gulp.start("image:project");
  });
  watch([path.watch.svg], function(event, cb) {
    gulp.start("svg:build");
  });
  watch([path.watch.fonts], function(event, cb) {
    gulp.start("fonts:build");
  });
});

// запуск сервера на прослушку
gulp.task("webserver", function() {
  browserSync(config);
});

// Очистка папки build
gulp.task("clean", function(cb) {
  rimraf(path.clean, cb);
});

// Запуск всей сборки
gulp.task("default", ["build", "webserver", "watch"]);
