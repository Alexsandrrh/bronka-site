var timeout = null;


// Sliders
//= components/swiper.min.js
//= components/swiper.js

//= ../../node_modules/jquery/dist/jquery.min.js
//= ../../node_modules/jquery-mousewheel/jquery.mousewheel.js
//= ../../node_modules/html5shiv/dist/html5shiv.min.js
//= ../../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js
//= ../../node_modules/svg4everybody/dist/svg4everybody.min.js
//= ../../node_modules/aos/dist/aos.js
//= ../../node_modules/imagesloaded/imagesloaded.pkgd.min.js
//= vendors/d3.js
//= vendors/queue.js

//= components/webgl/three.js
//= components/webgl/inflate.min.js
//= components/webgl/FBXLoader.js
//= components/webgl/OrbitControls.js
//= components/webgl/Lensflare.js
//= components/webgl/BokehShader2.js
//= components/webgl/Detector.js
//= components/webgl/detect.js
//= components/webgl/stats.min.js
//= components/webgl/main404.js

//= components/webgl/main2.js
//= components/webgl/main.js

//= vendors/jquery.fancybox.min.js
//= vendors/masonry/masonry.pkgd.min.js
//= vendors/masonry/isotope.pkgd.min.js
//= vendors/malihu-scroll.js
//= vendors/mask.min.js
//= vendors/validate.min.js
//= vendors/owl.carousel.js
//= vendors/jquery.spincrement.js

//= vendors/video/plyr.polyfilled.js
//= vendors/scrollme.js
//= vendors/parlx.js

//= components/header-index.js
//= components/menu.js
//= components/scrollto.js
//= components/owl.carousel.js
//= components/index-project.js
//= components/news.js
//= components/news-filter.js
//= components/svgScroll.js
//= components/header-menu-move.js
//= components/markerclusterer.js
//= components/ytplayer.js

//= components/map.js

//= components/preloader.js

$(document).ready(function($) {
  menuPanel.init();
  newsFilter.init();

  spanInName();

  $(".js-phone").mask("+7 (000) 000-00-00");

  $(".js-scroll-v").mCustomScrollbar();

  var videos = $(".video-js");

  // console.dir(new Plyr(".js-plur-video"));
  //
  // if (videos.length) {
  //   $.each(videos, function(i, item) {
  //     var player = videojs($(item).prop("id"));
  //   });
  // }

  setTimeout(function() {
    masonryBox();
  }, 100);

  jcb();
  $(window).on("resize", function() {
    jcb();
    masonryBox();
  });

  initGoogleMap();
  InitMultiMap();

  initPathAnimate();

  // initPlyrs();

  initPlyrsVideo();
});

function initPlyrsVideo() {
  var videosPlurs = $(".js-plur-video");

  videosPlurs.each(function() {
    var elemVideo = $(this);
    var parent = elemVideo.parent();
    var buttonPlay = elemVideo.parent().find(".js-plyr-video-btn");

    var plyr = new Plyr(elemVideo);
    //
    // plyr.on("pause", function() {
    //   parent.removeClass("-active");
    // });
    //
    // plyr.on("play", function() {
    //   parent.addClass("-active");
    // });

    buttonPlay.click(function() {
      parent.addClass("-active");

      console.log("[Plyr.js] : Click button play");

      plyr.play();
    });
  });
}

function masonryBox() {
  var windowWidth = $(window).width();

  $(".grid-container").isotope({
    percentPosition: true,
    itemSelector: ".grid-item",
    masonry: {
      columnWidth: ".grid-sizer"
    },
    getSortData: {
      ordered: function(itemElem) {
        var ordered = $(itemElem).data("order-sm");

        if (windowWidth > 1024) {
          ordered = $(itemElem).data("order-lg");
        }

        return ordered;
      }
    },
    sortBy: "ordered"
  });
}

function initPathAnimate() {
  if (!$("#pathAnimation").length) return false;
  var pathAnimationTop = $("#pathAnimation").offset().top;
  var windowHeight = window.innerHeight;
  var show = true;

  $(window).scroll(function() {
    if (show && pathAnimationTop < $(window).scrollTop() + windowHeight) {
      show = false;

      queue()
        .defer(d3.xml, "img/mapVector7.svg", "image/svg+xml")
        .await(ready);
    }
  });
}

function ready(error, xml) {
  if ($("#pathAnimation").length) {
    var importedNode = document.importNode(xml.documentElement, true);
    d3.select("#pathAnimation")
      .node()
      .appendChild(importedNode);

    var svg = d3.select("#pathAnimation svg"),
      svgWidth = svg.attr("width"),
      svgHeight = svg.attr("height");

    var paths = svg
      .selectAll("path")
      .transition()
      .duration(10000)
      .attrTween("stroke-dasharray", strokeDash);

    setTimeout(function() {
      $(".pathAnimation_bg").fadeIn(3000);
    }, 7000);

    function strokeDash() {
      var l = this.getTotalLength(),
        i = d3.interpolateString("0," + l, l + "," + l);

      return function(t) {
        return i(t);
      };
    }
  }
}

function jcb() {
  var wWidth = $(window).width();

  if (wWidth >= 1280) {
    var $jc = $(".jumbotron_content");
    if (!$jc.length) return false;

    var $jcb = $(".jumbotron_content_background");

    var jc_w = $jc.width();
    var jcb_w = jc_w + jc_w / 4;

    $jcb.width(jcb_w);
  }
}

function linkMoreWidth(box) {
  var wWidth = $(window).width();

  if (wWidth >= 1280) {
    var $boxNews = $(".press-center_news_item_more");
    var $linkMore = $(".press-center_block_preview_btn-more");

    var boxNewsWidth = box.outerWidth();
    $linkMore.width(boxNewsWidth - 90);
  }
}

function spincrement(elem) {
  var countBlockTop = $(elem).offset().top;
  var windowHeight = $(window).height();
  var show = true;

  run();

  $(window).scroll(function() {
    run();
  });

  function run() {
    if (show && countBlockTop < $(window).scrollTop() + windowHeight) {
      show = false;

      $(elem).spincrement({
        from: 0,
        duration: 2000,
        thousandSeparator: "",
        decimalPoint: ","
      });
    }
  }
}

function spanInName() {
  var $textBox = $(".js-fullname");

  $textBox.each(function(i, item) {
    var $box = $(item);
    var text = $box.text();
    var textArr = text.split(" ");

    $box.html("");
    for (let index = 0; index < textArr.length; index++) {
      const word = textArr[index];

      $box.append("<span>" + word + "</span>");
    }
  });
}

var piesiteFired = 0;
$(document).ready(function() {
  var $win = $(window),
    $win_height = $(window).height(),
    windowPercentage = $(window).height() * 0.9;
  $win.on("scroll", scrollReveal);

  function scrollReveal() {
    var scrolled = $win.scrollTop();

    ///////////////////////////////////////
    // Radial Graphs - scroll activate
    $(".piesite").each(function() {
      var $this = $(this),
        offsetTop = $this.offset().top;

      console.log(offsetTop);
      if (scrolled + windowPercentage > offsetTop || $win_height > offsetTop) {
        if (piesiteFired == 0) {
          timerSeconds = 3;
          timerFinish = new Date().getTime() + timerSeconds * 1000;
          $(".piesite").each(function(a) {
            pie = $("#pie_" + a).data("pie");
            timer = setInterval("stoppie(" + a + ", " + pie + ")", 0);
          });
          piesiteFired = 1;
        }
      } else {
        // $(".piesite").each(function() {
        //     piesiteFired = 0;
        // });
      }
    });
  }
  scrollReveal();
});

var timer;
var timerFinish;
var timerSeconds;

function drawTimer(c, a) {
  $("#pie_" + c).html(
    '<div class="percent"></div><div id="slice"' +
      (a > 50 ? ' class="gt50"' : "") +
      '><div class="pie"></div>' +
      (a > 50 ? '<div class="pie fill"></div>' : "") +
      "</div>"
  );
  var b = (360 / 100) * a;
  $("#pie_" + c + " #slice .pie").css({
    "-moz-transform": "rotate(" + b + "deg)",
    "-webkit-transform": "rotate(" + b + "deg)",
    "-o-transform": "rotate(" + b + "deg)",
    transform: "rotate(" + b + "deg)"
  });
  a = Math.floor(a * 100) / 100;
  arr = a.toString().split(".");
  intPart = arr[0];
}
function stoppie(d, b) {
  var c = (timerFinish - new Date().getTime()) / 1000;
  var a = 100 - (c / timerSeconds) * 100;
  a = Math.floor(a * 100) / 100;
  if (a <= b) {
    drawTimer(d, a);
  }
}

$(document).ready(function() {
  $("#owl-carousel").owlCarousel({
    loop: true,
    autoplay: true,
    smartSpeed: 1000,
    autoplayTimeout: 2000,
    dots: true,
    items: 1,
    dotData: true,
    autoplayHoverPause: true
  });
  $("#owl-carousel2").owlCarousel({
    loop: true,
    autoplay: true,
    smartSpeed: 1000,
    autoplayTimeout: 2000,
    dots: true,
    items: 1,
    dotData: true,
    autoplayHoverPause: true
  });
});
