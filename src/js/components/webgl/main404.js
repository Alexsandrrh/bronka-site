if (document.getElementById('space-container404')) {
    if (!Detector.webgl) Detector.addGetWebGLMessage();

    var container, stats, controls;
    var camera, scene, renderer;

    var mobdev = isMobileDevice();

    renderer = new THREE.WebGLRenderer({ antialias: false, powerPreference: "high-performance" });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    //renderer.shadowMap.enabled = true;

    camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 100000);

    //camera = new THREE.CinematicCamera( 60, window.innerWidth / window.innerHeight, 1, 100000 );
    //camera.setLens( 10 );

    var ua = detect.parse(navigator.userAgent);

    var a;
    function isChrome(){
      if (((ua.browser.family == 'Chrome Mobile iOS' ||  ua.browser.family == 'Chrome Mobile') && ua.browser.version < 76) || (ua.browser.family == 'Mobile Safari') && (ua.browser.version.substring(0, 2) < 13)){
        a = true;
        
      } else {a=false;};
    }; isChrome();

    //alert(ua.browser.family);
    //alert(ua.browser.version);


    if (mobdev){
      camera.position.set( 100, 0, 1060 ); // x z y
    } else{
      camera.position.set( 67400, 1400, 1060 ); // x z y
    };

    if (a){
      camera.position.set( 0, 0, 1060 ); // x z y
    };


          
    if (mobdev && !a){
      controls = new THREE.OrbitControls( camera, renderer.domElement );

      controls.enableDamping = true;
      controls.dampingFactor = 0.05;
      controls.screenSpacePanning = false;
      //controls.minDistance = 100;
      //controls.maxDistance = 500;
      controls.maxPolarAngle = 2;
      controls.minPolarAngle  = 1.25 ;

      controls.maxAzimuthAngle = 1;
      controls.minAzimuthAngle = -1;
      
      controls.enableZoom = false; 
      controls.rotateSpeed = 0.02; // 0.002
      controls.enablePan = false;
      controls.enabled = true;

    };

    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0x000000 );

    //LIGHT

    var lightA = new THREE.AmbientLight(0xffffff); // soft white light
    scene.add(lightA);

    //CURSOR CONTROL
    var mouseX = 0,
        mouseY = 0;
    var windowHalfX = window.innerWidth / 2;
    var windowHalfY = window.innerHeight / 2;
    document.addEventListener('mousemove', onDocumentMouseMove, false);


    function onDocumentMouseMove(event) {
        mouseX = (event.clientX - windowHalfX);
        mouseY = (event.clientY - windowHalfY);
    }

    //LENSFLARE

    var dirLight = new THREE.DirectionalLight(0xffffff, 0.01);
    dirLight.position.set(0, 0, 0).normalize();
    dirLight.color.setHSL(0.99, 0.99, 0.99);
    scene.add(dirLight);
    // lensflares
    var textureLoader = new THREE.TextureLoader();
    var textureFlare0 = textureLoader.load('models/textures/lensflare5.png');
    var textureFlare1 = textureLoader.load('models/textures/lensflare6.png');
    var textureFlare2 = textureLoader.load('models/textures/lensflare7.png');
    var textureFlare3 = textureLoader.load('models/textures/lensflare8.png');
    addLight(0.995, 0.99, 0.99, 1000, 12000, -50000);

    function addLight(h, s, l, x, y, z) {
        var light = new THREE.PointLight(0xffffff, 3.5, 2000);
        light.color.setHSL(h, s, l);
        light.position.set(x, y, z);
        scene.add(light);
        var lensflare = new THREE.Lensflare();
        lensflare.addElement( new THREE.LensflareElement( textureFlare0, 1500, 0, light.color ) );
        lensflare.addElement( new THREE.LensflareElement( textureFlare3, 270, - 0.3 ) );
        lensflare.addElement( new THREE.LensflareElement( textureFlare2, 110, - 0.2 ) );
        lensflare.addElement( new THREE.LensflareElement( textureFlare2, 120, - 0.1 ) );
        lensflare.addElement( new THREE.LensflareElement( textureFlare1, 110, 0.1 ) );
        lensflare.addElement( new THREE.LensflareElement( textureFlare3, 120, 0.2 ) );
        lensflare.addElement( new THREE.LensflareElement( textureFlare1, 270, 0.3 ) );
        lensflare.addElement( new THREE.LensflareElement( textureFlare2, 110, 0.4 ) );
        lensflare.addElement( new THREE.LensflareElement( textureFlare2, 120, 0.5 ) );
        lensflare.addElement( new THREE.LensflareElement( textureFlare3, 270, 0.6 ) );
        lensflare.addElement( new THREE.LensflareElement( textureFlare1, 110, 0.7 ) );
        lensflare.addElement( new THREE.LensflareElement( textureFlare2, 60, 0.8 ) );
        lensflare.addElement( new THREE.LensflareElement( textureFlare3, 420, 0.9 ) );
        lensflare.addElement( new THREE.LensflareElement( textureFlare3, 30, 1 ) );
        light.add(lensflare);
    };

    if (mobdev) {

        // model mobile
        var loader = new THREE.FBXLoader();
        loader.load('models/bronka404.fbx', function(object) {
            scene.add(object);

            var clouds = textureLoader.load('models/textures/clouds.png');
            var materialcl = new THREE.MeshBasicMaterial({ map: clouds, transparent: true });
            var cl = scene.getObjectByName('clouds');
            cl.material = materialcl;

            function animatep() {
                requestAnimationFrame(animatep);
                cl.rotation.x += 0.00025;
                cl.rotation.y += 0.00025;
                cl.rotation.z += 0.00025;
            }
            animatep();

        });

    } else {

        // model desctop
        var loader = new THREE.FBXLoader();
        loader.load('models/bronka404.fbx', function(object) {
            scene.add(object);

            var clouds = textureLoader.load('models/textures/clouds.png');
            var materialcl = new THREE.MeshBasicMaterial({ map: clouds, transparent: true });
            var cl = scene.getObjectByName('clouds');
            cl.material = materialcl;

            function animatep() {
                requestAnimationFrame(animatep);
                cl.rotation.x += 0.00025;
                cl.rotation.y += 0.00025;
                cl.rotation.z += 0.00025;
            }
            animatep();

        });

    };


    init();
    animate();

    function init() {
        container = document.getElementById('space-container404')

        if (!container) return false;
        // container = document.createElement('div');
        // document.getElementById("canvas").appendChild(container);
        document.getElementById("canvas404").appendChild(renderer.domElement);
        // container.appendChild(renderer.domElement);
        window.addEventListener('resize', onWindowResize, false);
        console.log('init');

        //stats = new Stats();
        //container.appendChild( stats.dom );



    };

    function isMobileDevice() {
        return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
    };


    function onWindowResize() {
        windowHalfX = window.innerWidth / 2;
        windowHalfY = window.innerHeight / 2;
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
    }

    function onDocumentMouseMove(event) {
        mouseX = (event.clientX - windowHalfX);
        mouseY = (event.clientY - windowHalfY);
    };

    function animate() {
      requestAnimationFrame( animate );
    
      if (mobdev && !a){
        controls.update();
      };
    
    
      render();
    
    };


    function render() {

      if (a) {
        window.addEventListener( "deviceorientation", function(event) {
          
          camera.position.x += ( event.gamma * 10 - camera.position.x ) * 0.001;
          camera.position.y += ( event.beta * 4 - camera.position.y ) *( ( event.beta < 10 ? 0 : 0.001) && ( event.beta > 68 ? 0 : 0.001 ) );
          //infoLog.innerHTML = event.beta;
    
        }, true);
      }; 
    
      if (!mobdev){
    
        camera.position.y += ( - mouseY - camera.position.y ) * 0.03;
        
        if ( mouseX > 0 ) {
            camera.position.x += ( mouseX/2.5 - camera.position.x ) * 0.03;
    
        } else {
            camera.position.x += ( mouseX - camera.position.x ) * 0.03;
        };
      };

      camera.lookAt(new THREE.Vector3(0, 100, 0));
      renderer.render(scene, camera);
    };
}