$(document).ready(function() {
    var menuPanel = {
        init: function() {
            this.body = $('body');
            this.container = $('.header_menu');
            this.buttonHamburger = $('.js-hamburger');
            this.outerFull = $('.outer');

            this.classOpen = 'menu-open';
            this.classActive = 'is-active';

            this.isOpen = false;

            this._initEvents();
            this._scroll()
        },

        _initEvents: function() {
            var self = this;
            var listItems = $('#navigation').find('li');

            self.buttonHamburger.on('click', function() {
                self.isOpen = !self.isOpen;
                self._stateMenu();

                return false;
            })

            self.outerFull.on('click', function() {
                if ($(window).width() <= 1024) {
                    if ($(this).hasClass('hover')) {
                        $(this).removeClass('hover');
                    } else {
                        listItems.removeClass('hover');
                        $(this).addClass('hover');
                    }
                }
            })

            self.container.on('click', function() {
                console.log('self.container')
                    // $('.header_menu').mCustomScrollbar('update')
            });
        },

        _stateMenu: function() {
            var self = this;
            var dropdown = document.querySelector('.dropdown-menu');
            if (self.isOpen) {
                self.body.addClass(self.classOpen)
                self.buttonHamburger.addClass(self.classActive)
                if (dropdown) {
                    dropdown.classList.remove('show');
                }
            } else {
                self.body.removeClass(self.classOpen)
                self.buttonHamburger.removeClass(self.classActive)
            }

            self._scroll();
        },

        _scroll: function() {
            var self = this;
            console.log('_scroll')
            if ($(window).width() <= 1024) {
                $('.header_menu').mCustomScrollbar({
                    setHeight: $(window).height()
                });
            }

        }
    };
    window.menuPanel = menuPanel;
});