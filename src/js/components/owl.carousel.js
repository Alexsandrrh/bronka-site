window.addEventListener("DOMContentLoaded", function() {
  var $carousels = $(".js-carousel");
  var $carouselsActives = $(".actives_item_photoarchive");
  var $carouselsPhotos = $(".js-carousel-photos");
  var $carouselsVideos = $(".video-box_carousel");
  var $links = $(".video-box-menu li");

  $links.on("click", function(e) {
    $($links).each(function() {
      $(this).removeClass("active");
    });
    $(this).addClass("active");
  });

  if ($carousels.length) {
    $carousels.each(function(i, item) {
      var carousel = $(item);
      var owl = carousel.find(".owl-carousel");

      var owlResponsive = owl.data("responsive")
        ? owl.data("responsive")
        : {
            0: {
              items: 1
            },
            760: {
              items: 2
            },
            1280: {
              items: 3
            },
            1600: {
              items: 3
            }
          };

      owl.owlCarousel({
        loop: owl.data("loop") ? owl.data("loop") : false,
        margin: 0,
        responsiveClass: true,
        nav: false,
        dots: owl.data("dots") ? owl.data("dots") : false,
        autoplay: owl.data("autoplay") ? owl.data("autoplay") : false,
        autoplayTimeout: owl.data("autoplaytimeout")
          ? owl.data("autoplaytimeout")
          : 5000,
        autoplayHoverPause: true,
        smartSpeed: 500,
        responsive: owlResponsive,
        autoHeight: owl.data("autoheight") ? owl.data("autoheight") : false,
        onInitialized: function(event) {
          disableButton(event);
        }
      });

      owl.on("changed.owl.carousel", function(event) {
        var $parent = $(event.currentTarget);
        disableButton(event);

        if ($parent.hasClass("press-center_block_carausel")) {
          setLinks(owl, event);
        }
      });

      owl.on("resized.owl.carousel", function(event) {
        // initPositionNav(owl);
      });

      carousel.find(".owl-buttons_link-next").click(function() {
        owl.trigger("next.owl.carousel");
      });
      carousel.find(".owl-buttons_link-prev").click(function() {
        owl.trigger("prev.owl.carousel");
      });

      function disableButton(event) {
        if (event.relatedTarget.options.loop) return false;

        var point = Math.ceil(event.item.count / event.page.size);
        var index = event.item.index + 1;

        carousel.find(".owl-buttons_link").addClass("disable");

        if (point > index) {
          carousel.find(".owl-buttons_link-next").removeClass("disable");
        }
        if (index > 1) {
          carousel.find(".owl-buttons_link-prev").removeClass("disable");
        }

        carousel.find(".js-pages-current").html(index);
        carousel.find(".js-pages-total").html(event.item.count);

        if (event.item.count == event.page.size) {
          carousel.find(".owl-buttons").hide();
        } else {
          carousel.find(".owl-buttons").show();
        }
      }

      function initPositionNav(owl) {
        var owlItemPreview = owl.find(".product-box_image");
        if (!owlItemPreview.length) return false;

        var previewHeight = Math.ceil(owlItemPreview.height() / 2);

        carousel.find(".carousel_nav_link").css({
          top: previewHeight - 20 + "px"
        });
      }

      function setLinks(owl, event) {
        var index = event.item.index - 1;
        var $globalLink = $(".press-center_block_preview_btn-more");
        var $items = owl.find(".press-center_block_carausel_item");
        var $itemCurrent = $items.eq(index);
        var $itemCurrentLink = $itemCurrent.find(
          ".press-center_block_carausel_btn"
        );

        $globalLink.attr("href", $itemCurrentLink.attr("href"));
      }
    });
  }

  if ($carouselsActives.length) {
    var o1 = $("#photoarchive1");
    var o2 = $("#photoarchive2");
    var menuActives = $(".actives_item_photoarchive_menu li");

    var currentItem = o1.data("start") ? o1.data("start") : 0;

    o1.owlCarousel({
      items: 1,
      singleItem: true,
      loop: true,
      margin: 0,
      startPosition: currentItem,
      autoplay: o1.data("autoplay") ? o1.data("autoplay") : true,
      autoplayTimeout: o1.data("autoplaytimeout")
        ? o1.data("autoplaytimeout")
        : 5000,
      autoplayHoverPause: true,
      smartSpeed: 500,
      dots: o1.data("dots") ? o1.data("dots") : false,
      pagination: false,
      navigation: false,
      touchDrag: false,
      mouseDrag: false,
      autoHeight: true,
      onInitialized: function(event) {
        setCurrentMenu(currentItem);
      },
      dotsContainer: $(".actives_item_photoarchive_menu ul")
    });

    o1.on("changed.owl.carousel", function(event) {
      var item = o1
        .find(".actives_item_photoarchive_media_carousel_item")
        .eq(event.item.index);
      currentItem = item.data("number");
      // var item2 = o1.find('.actives_item_photoarchive_menu li').eq(event.item.index);
      // currentItem = item2.data('hash');

      setTimeout(function() {
        o2.trigger("to.owl.carousel", currentItem, 0);
        setCurrentMenu(currentItem);
      }, 0);
    });

    $carouselsActives.find(".js-media-next").on("click", function() {
      o1.trigger("next.owl.carousel");
    });
    $carouselsActives.find(".js-media-prev").on("click", function() {
      o1.trigger("prev.owl.carousel");
    });

    $(".actives_item_photoarchive_media_carousel-controls").on(
      "mouseover",
      function() {
        o1.trigger("stop.owl.autoplay");
      }
    );

    $(".actives_item_photoarchive_media_carousel-controls").on(
      "mouseout",
      function() {
        o1.trigger("play.owl.autoplay");
      }
    );

    $(".actives_item_photoarchive_menu ul").on("mouseover", function() {
      o1.trigger("stop.owl.autoplay");
    });

    $(".actives_item_photoarchive_menu ul").on("mouseout", function() {
      o1.trigger("play.owl.autoplay");
    });

    o2.owlCarousel({
      items: 1,
      singleItem: true,
      loop: true,
      margin: 10,
      dots: false,
      pagination: false,
      navigation: false,
      touchDrag: false,
      mouseDrag: false,
      animateOut: false,
      //'fadeOut',
      autoHeight: true
    });

    function setCurrentMenu(currentItem) {
      menuActives
        .removeClass("current")
        .eq(currentItem)
        .addClass("current");
    }
  }

  if ($carouselsPhotos.length) {
    $carouselsPhotos.each(function(i, item) {
      var carousel = $(item);
      var owl = carousel.find(".owl-carousel");
      var $previews = $(".photo_previews_item");

      var owlResponsive = owl.data("responsive")
        ? owl.data("responsive")
        : {
            0: {
              items: 1
            },
            760: {
              items: 2
            },
            1280: {
              items: 3
            },
            1600: {
              items: 3
            }
          };

      owl.owlCarousel({
        loop: owl.data("loop") ? owl.data("loop") : false,
        margin: 0,
        responsiveClass: true,
        nav: false,
        dots: owl.data("dots") ? owl.data("dots") : false,
        autoplay: owl.data("autoplay") ? owl.data("autoplay") : true,
        autoplayTimeout: owl.data("autoplaytimeout")
          ? owl.data("autoplaytimeout")
          : 5000,
        autoplayHoverPause: true,
        smartSpeed: 500,
        responsive: owlResponsive,
        autoHeight: owl.data("autoheight") ? owl.data("autoheight") : false,
        onInitialized: function(event) {
          disableButton(event);
        }
      });

      owl.on("changed.owl.carousel", function(event) {
        var $parent = $(event.currentTarget);
        disableButton(event);

        if ($parent.hasClass("press-center_block_carausel")) {
          setLinks(owl, event);
        }
      });

      owl.on("resized.owl.carousel", function(event) {
        // initPositionNav(owl);
      });

      carousel.find(".owl-buttons_link-next").click(function() {
        owl.trigger("next.owl.carousel");
      });
      carousel.find(".owl-buttons_link-prev").click(function() {
        owl.trigger("prev.owl.carousel");
      });

      function disableButton(event) {
        if (event.relatedTarget.options.loop) return false;

        var point = Math.ceil(event.item.count / event.page.size);
        var index = event.item.index + 1;

        currentPreview(event.item.index);

        carousel.find(".owl-buttons_link").addClass("disable");

        if (point > index) {
          carousel.find(".owl-buttons_link-next").removeClass("disable");
        }
        if (index > 1) {
          carousel.find(".owl-buttons_link-prev").removeClass("disable");
        }

        carousel.find(".js-pages-current").html(index);
        carousel.find(".js-pages-total").html(event.item.count);

        if (event.item.count == event.page.size) {
          carousel.find(".owl-buttons").hide();
        } else {
          carousel.find(".owl-buttons").show();
        }
      }

      function currentPreview(index) {
        $previews
          .removeClass("current")
          .eq(index)
          .addClass("current");
      }

      $previews.on("click", function() {
        var index = $(this).index();

        owl.trigger("to.owl.carousel", index);

        return false;
      });
    });
  }

  if ($carouselsVideos.length) {
    $carouselsVideos.each(function(i, item) {
      var carousel = $(item);
      var owl = carousel.find(".owl-carousel");

      console.log(owl);

      var owlResponsive = {
        0: {
          items: 1
        }
      };

      var options = {
        loop: owl.data("loop") ? owl.data("loop") : false,
        margin: 0,
        responsiveClass: true,
        nav: false,
        dots: owl.data("dots") ? owl.data("dots") : false,
        autoplay: owl.data("autoplay") ? owl.data("autoplay") : false,
        autoplayTimeout: owl.data("autoplaytimeout")
          ? owl.data("autoplaytimeout")
          : 5000,
        dotsContainer: $(".video-box-menu ul"),

        autoplayHoverPause: true,
        smartSpeed: 500,
        responsive: owlResponsive,
        URLhashListener: true,
        autoHeight: owl.data("autoheight") ? owl.data("autoheight") : false,
        onInitialized: function(event) {
          disableButton(event);
          // initPlyrs();
        }
      };

      owl.owlCarousel(options);

      owl.on("changed.owl.carousel", function(event) {
        var $parent = $(event.currentTarget);
        disableButton(event);

        if ($parent.hasClass("press-center_block_carausel")) {
          setLinks(owl, event);
        }
      });

      owl.on("resized.owl.carousel", function(event) {
        // initPositionNav(owl);
      });

      carousel.find(".owl-buttons_link-next").click(function() {
        owl.trigger("next.owl.carousel");
      });
      carousel.find(".owl-buttons_link-prev").click(function() {
        owl.trigger("prev.owl.carousel");
      });

      $(".video-box-menu ul").on("mouseover", function() {
        owl.trigger("stop.owl.autoplay");
      });

      $(".video-box-menu ul").on("mouseout", function() {
        owl.trigger("play.owl.autoplay");
      });

      owl.on("resized.owl.carousel", function(event) {
        // initPositionNav(owl);
      });

      carousel.find(".owl-buttons_link-next").click(function() {
        owl.trigger("next.owl.carousel");
      });
      carousel.find(".owl-buttons_link-prev").click(function() {
        owl.trigger("prev.owl.carousel");
      });

      function disableButton(event) {
        if (event.relatedTarget.options.loop) return false;

        var point = Math.ceil(event.item.count / event.page.size);
        var index = event.item.index + 1;

        carousel.find(".owl-buttons_link").addClass("disable");

        if (point > index) {
          carousel.find(".owl-buttons_link-next").removeClass("disable");
        }
        if (index > 1) {
          carousel.find(".owl-buttons_link-prev").removeClass("disable");
        }

        carousel.find(".js-pages-current").html(index);
        carousel.find(".js-pages-total").html(event.item.count);

        if (event.item.count == event.page.size) {
          carousel.find(".owl-buttons").hide();
        } else {
          carousel.find(".owl-buttons").show();
        }
      }
    });
  }
});
