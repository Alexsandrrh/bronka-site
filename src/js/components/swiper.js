window.addEventListener("DOMContentLoaded", function() {
  var swiper = new Swiper(".swiper-container", {
    slidesPerView: 1,
    loop: true,
    spaceBetween: 0,
    updateOnWindowResize: true,
    setWrapperSize: true,
    effect: "fade",
    fadeEffect: {
      crossFade: true
    },
    autoplay: {
      delay: 2000,
      disableOnInteraction: false
    },
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    }
  });

  new Swiper(".swiper-container-press", {
    slidesPerView: 1,
    loop: true,
    spaceBetween: 0,
    updateOnWindowResize: true,
    setWrapperSize: true,
    autoplay: {
      delay: 2500,
      disableOnInteraction: false
    },
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    }
  });

  var galleryThumbs = new Swiper(".gallery-thumbs", {
    spaceBetween: 5,
    slidesPerView: 8,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true
  });
  var galleryTop = new Swiper(".gallery-top", {
    spaceBetween: 10,
    slidesPerView: 1,
    loop: true,
    updateOnWindowResize: true,
    setWrapperSize: true,
    autoplay: {
      delay: 2500,
      disableOnInteraction: false
    },
    thumbs: {
      swiper: galleryThumbs
    }
  });
});
