$(document).ready(function($) {
    var $indexProjects = $('.news-index_item');

    if($indexProjects.length) {
        $indexProjects.on('mouseover', function() {
            var $video = $(this).find('video');
            if($video.length) $video[0].play()
        }).on('mouseout', function() {
            var $video = $(this).find('video');
            if($video.length) $video[0].pause()
        })
    }
});