$(document).ready(function () {
    var newsFilter = {
        init: function () {
            this.body = $('body');
            this.containers = $('.filter-news-all');
            this.links = $('.js-filter-news-btn');

            this.classCurrent = 'current';
            this.classShow = 'show';
            this.timeout = null;
            this.currentIndex = 'all';

            if (!this.containers.length)
                return false;

            this._initEvents();
            this._state();
        },

        _initEvents: function () {
            var self = this;

            self.links.on('click', function () {
                var elem = $(this);
                var parent = elem.parents('.dropdown');
                clearTimeout(self.timeout);

                parent.removeClass(self.classShow);
                parent.find('.dropdown-toggle').find('span').html(elem.html());
                parent.find('.dropdown-menu').removeClass(self.classShow);

                timeout = setTimeout(function () {
                    self.currentIndex = elem.data('filter-show');

                    $('html, body').animate({
                        scrollTop: $(elem.attr('href')).offset().top
                    }, 1000);

                    self._state()
                }, 300)
                return false;
            })

            $(window).on('resize', function () {
                clearTimeout(self.timeout);
                timeout = setTimeout(function () {
                    self._state()
                }, 300)
            })
        },

        _state: function () {
            var self = this;
            var wWindow = $(window).width();

            self.containers.hide();

            if (wWindow > 760 && wWindow < 1270) {
                $('.js-filter-news-' + self.currentIndex).show();
                // $('.js-filter-news-' + self.currentIndex).eq(2).show();
                // $('.js-filter-news-' + self.currentIndex).eq(4).show();
            } else if (wWindow > 1270 && wWindow < 1590) {
                $('.js-filter-news-' + self.currentIndex).show();
                // $('.js-filter-news-' + self.currentIndex).eq(1).show();
                // $('.js-filter-news-' + self.currentIndex).eq(2).show();
                // $('.js-filter-news-' + self.currentIndex).eq(4).show();
            } else {
                $('.js-filter-news-' + self.currentIndex).show();
            }

            self.links.removeClass(self.classCurrent);
            $('[data-filter-show="' + self.currentIndex + '"]').addClass(self.classCurrent);
            if (wWindow > 1600) {
                linkMoreWidth($('.js-filter-news-' + self.currentIndex).eq(4));
            }
        }
    };
    window.newsFilter = newsFilter;
});