// let player;

// const formatTime = timeSec => {
//     const roundTime = Math.round(timeSec);

//     const minutes = Math.floor(roundTime / 60);
//     const seconds = roundTime - minutes * 60;

//     const formattedSeconds = seconds < 10 ? `0${seconds}` : seconds;

//     return `${minutes}:${formattedSeconds}`;
// };

// const onPlayerReady = () => {
//     let interval;
//     let durationSec = player.getDuration();

//     $(".player__duration-estimate").text(formatTime(durationSec));

//     if (typeof interval !== "undefined") {
//         clearInterval(interval);
//     }

//     interval = setInterval(() => {
//         const completedSec = player.getCurrentTime();
//         const completedPercent = (completedSec / durationSec) * 100;

//         $(".player__playback-button").css({
//             left: `${completedPercent}%`
//         });

//         $(".player__duration-completed").text(formatTime(completedSec));
//     }, 1000);
// };

// const eventsInit = () => {
//     $(".player__start").on("click", e => {
//         e.preventDefault();
//         const btn = $(e.currentTarget);

//         if (btn.hasClass("paused")) {
//             player.pauseVideo();
//         } else {
//             player.playVideo();
//         }
//     });

//     $(".player__playback").on("click", e => {
//         const bar = $(e.currentTarget);
//         const newButtonPosition = e.pageX - bar.offset().left;
//         const buttonPosPercent = (newButtonPosition / bar.width()) * 100;
//         const newPlayerTimeSec = (player.getDuration() / 100) * buttonPosPercent;

//         $(".player__playback-button").css({
//             left: `${buttonPosPercent}%`
//         });

//         player.seekTo(newPlayerTimeSec);
//     });

//     $(".player__splash").on("click", e => {
//         player.playVideo();
//     });
// };

// const onPlayerStateChange = event => {
//     const playerButton = $(".player__start");
//     /*
//     -1 (воспроизведение видео не начато)
//     0 (воспроизведение видео завершено)
//     1 (воспроизведение)
//     2 (пауза)
//     3 (буферизация)
//     5 (видео подают реплики).
//      */
//     switch (event.data) {
//         case 1:
//             $('.player__wrapper').addClass('active');
//             playerButton.addClass("paused");
//             break;
//         case 2:
//             playerButton.removeClass("paused");
//             break;
//     }
// };

// function onYouTubeIframeAPIReady() {
//     player = new YT.Player("yt-player", {
//         height: "405",
//         width: "660",
//         videoId: "p-YeIcxpxu8",
//         events: {
//             onReady: onPlayerReady,
//             onStateChange: onPlayerStateChange
//         },
//         playerVars: {
//             controls: 0,
//             disablekb: 0,
//             showinfo: 0,
//             rel: 0,
//             autoplay: 0,
//             modestbranding: 0
//         }
//     });
// }

// eventsInit();

//youtube script

// !!!!!!!

// const videos = document.querySelector(".page-menu--videos");
//
// function createElement(tagName, classNames) {
//   const elem = document.createElement(tagName);
//
//   if (Array.isArray(classNames)) {
//     elem.classList.add(...classNames);
//   } else {
//     elem.classList.add(classNames);
//   }
//
//   return elem;
// }
//
// if (videos) {

function onYouTubeIframeAPIReady() {
  const players = [];
  const videoBankMainBlock = $(".js-block-videos");

  videoBankMainBlock.find(".js-video-box").each(function() {
    const itemVideoBox = $(this);
    const itemVideoBoxData = itemVideoBox.data();

    const playerBlock = itemVideoBox.find(".player");
    const previewBlock = itemVideoBox.find(".thumbnail_container");
    const playButton = itemVideoBox.find(".play");

    previewBlock.css({
      backgroundImage: 'url("' + itemVideoBoxData.poster + '")'
    });

    const player = new YT.Player(playerBlock[0], {
      height: "100%",
      width: "100%",
      videoId: itemVideoBoxData.video,
      events: {
        onStateChange: function(e) {
          console.log(e);

          switch (e.data) {
            case YT.PlayerState.PLAYING:
              itemVideoBox.addClass("-hidden");
              break;

            case YT.PlayerState.BUFFERING:
              itemVideoBox.addClass("-hidden");
              break;

            case YT.PlayerState.PAUSED:
              itemVideoBox.removeClass("-hidden");
              itemVideoBox.addClass("-hidden-blur");

              setTimeout(() => {
                videoBankMainBlock.trigger("play.owl.autoplay");
                console.log("[YT.Player] : Start slider with videos");
              }, 1500);

              break;
            case YT.PlayerState.ENDED:
              itemVideoBox.removeClass("-hidden");
              break;
          }
        }
      }
    });

    function onClick(e) {
      itemVideoBox.addClass("-hidden");

      console.log("[YT.Player] : Stopped slider");

      players.forEach(playerItem => {
        playerItem.stopVideo();
      });
      console.log("[YT.Player] : Stopped all videos");

      const currentTime = player.getCurrentTime();

      console.log(currentTime);

      player.playVideo();

      if (currentTime !== 0) {
        player.seekTo(currentTime);
      }

      console.log("[YT.Player] : Play video: " + itemVideoBoxData.video);

      console.dir(player);

      videoBankMainBlock.trigger("stop.owl.autoplay");
      console.log("[YT.Player] : Get parent block");
    }

    playButton.click(onClick);
  });
}

//   const previewAPIUrl = "https://i.ytimg.com/vi";
//
//   const listVideos = [
//     "nO7jjpWEt8k",
//     "nO7jjpWEt8k",
//     "nO7jjpWEt8k",
//     "nO7jjpWEt8k",
//     "nO7jjpWEt8k",
//     "nO7jjpWEt8k",
//     "nO7jjpWEt8k"
//   ];
//
//   const main = createElement("div", "owl-carousel");
//   main.setAttribute("data-autoplay", "true");
//   main.setAttribute("data-autoplaytimeout", "5000");
//   main.setAttribute("data-loop", "true");
//   main.setAttribute("data-dots", "true");
//
//   const htmlElements = listVideos.map((video, index) => {
//     const preview = `${previewAPIUrl}/${video}/maxresdefault.jpg`;
//
//     const mainBlock = createElement("div", [
//       "video-box_preview",
//       "video-box_preview--page"
//     ]);
//     mainBlock.setAttribute("data-poster", preview);
//     mainBlock.setAttribute("data-hash", `0${++index}`);
//
//     const previewBlock = createElement("div", ["thumbnail_container"]);
//
//     previewBlock.style.backgroundImage = `url("${preview}")`;
//
//     const playerBlock = createElement("div", "player");
//
//     const button = createElement("button", ["play"]);
//
//     const buttonContent = `<p class="play-text">Смотреть видео</p>
//                                 <span>
//                                     <svg class="icon icon-play">
//                                         <use xlink:href="svg/sprite.svg#play"></use>
//                                     </svg>
//                                  </span>`;
//
//     button.innerHTML = buttonContent;
//
//     mainBlock.append(previewBlock);
//     mainBlock.append(playerBlock);
//     mainBlock.append(button);
//
//     main.append(mainBlock);
//
//     return {
//       video,
//       mainBlock,
//       previewBlock,
//       playerBlock,
//       button
//     };
//   });
//
//   document.querySelector(".video-box_carousel").append(main);
//
//   var point = document.querySelectorAll(".video-box-menu-list li");
//
//   onYouTubeIframeAPIReady = function() {
//     let clicked = false;
//
//     htmlElements.forEach(
//       ({ mainBlock, playerBlock, previewBlock, button, video }) => {
//         const player = new YT.Player(playerBlock, {
//           height: "100%",
//           width: "100%",
//           videoId: video,
//           events: {
//             onStateChange: function(e) {
//               console.log(e);
//               switch (e.data) {
//                 case YT.PlayerState.PAUSED:
//                   mainBlock.classList.remove("-hidden");
//                   mainBlock.classList.add("-hidden-blur");
//                   break;
//                 case YT.PlayerState.ENDED:
//                   mainBlock.classList.remove("-hidden");
//                   break;
//               }
//             }
//           }
//         });
//
//         function onClick(e) {
//           mainBlock.classList.add("-hidden");
//
//           player.playVideo();
//         }
//
//         previewBlock.onclick = onClick;
//         button.onclick = onClick;
//       }
//     );
//
//     // onPlayerStateChange = function(event) {
//     //   if (event.data == YT.PlayerState.ENDED) {
//     //     $(".play").fadeIn("normal");
//     //   }
//     // };
//   };
// }

// var player2;

// onYouTubeIframeAPIReady = function() {
//     player2 = new YT.Player('player2', {
//         height: '244',
//         width: '434',
//         videoId: 'AkyQgpqRyBY', // youtube video id
//         playerVars: {
//             'autoplay': 0,
//             'rel': 0,
//             'showinfo': 0
//         },
//         events: {
//             'onStateChange': onPlayerStateChange
//         }
//     });
// }

// var p = document.getElementById("player2");
// $(p).hide();

// var t = document.getElementById("thumbnail2");
// t.src = "img/content/video-preview.jpg";

// onPlayerStateChange = function(event) {
//     if (event.data == YT.PlayerState.ENDED) {
//         $('.play').fadeIn('normal');
//     }
// }

// $(document).on('click', '#play2', function() {
//     $(this).hide();
//     $("#player2").show();
//     $("#thumbnail_container2").hide();
//     // player2.playVideo();
// });

// var player3;

// onYouTubeIframeAPIReady = function() {
//     player3 = new YT.Player('player3', {
//         height: '244',
//         width: '434',
//         videoId: 'AkyQgpqRyBY', // youtube video id
//         playerVars: {
//             'autoplay': 0,
//             'rel': 0,
//             'showinfo': 0
//         },
//         events: {
//             'onStateChange': onPlayerStateChange
//         }
//     });
// }

// var p = document.getElementById("player3");
// $(p).hide();

// var t = document.getElementById("thumbnail3");
// t.src = "img/content/video-preview.jpg";

// onPlayerStateChange = function(event) {
//     if (event.data == YT.PlayerState.ENDED) {
//         $('.play').fadeIn('normal');
//     }
// }

// $(document).on('click', '#play3', function() {
//     $(this).hide();
//     $("#player3").show();
//     $("#thumbnail_container3").hide();
//     // player3.playVideo();
// });

// var player4;

// onYouTubeIframeAPIReady = function() {
//     player4 = new YT.Player('player4', {
//         height: '244',
//         width: '434',
//         videoId: 'AkyQgpqRyBY', // youtube video id
//         playerVars: {
//             'autoplay': 0,
//             'rel': 0,
//             'showinfo': 0
//         },
//         events: {
//             'onStateChange': onPlayerStateChange
//         }
//     });
// }

// var p = document.getElementById("player4");
// $(p).hide();

// var t = document.getElementById("thumbnail4");
// var tc = document.getElementById("thumbnail_container4");
// t.src = "img/content/video-preview.jpg";

// onPlayerStateChange = function(event) {
//     if (event.data == YT.PlayerState.ENDED) {
//         $('.play').fadeIn('normal');
//     }
// }

// $(document).on('click', '.play', function() {
//     $(this).hide();
//     $("#player4").show();
//     $("#thumbnail_container4").hide();
//     // player4.playVideo();
// });

// var player5;

// onYouTubeIframeAPIReady = function() {
//     player5 = new YT.Player('player5', {
//         height: '244',
//         width: '434',
//         videoId: 'AkyQgpqRyBY', // youtube video id
//         playerVars: {
//             'autoplay': 0,
//             'rel': 0,
//             'showinfo': 0
//         },
//         events: {
//             'onStateChange': onPlayerStateChange
//         }
//     });
// }

// var p = document.getElementById("player5");
// $(p).hide();

// var t = document.getElementById("thumbnail5");
// t.src = "img/content/video-preview.jpg";

// onPlayerStateChange = function(event) {
//     if (event.data == YT.PlayerState.ENDED) {
//         $('.play').fadeIn('normal');
//     }
// }

// $(document).on('click', '#play5', function() {
//     $(this).hide();
//     $(".player5").show();
//     $("#thumbnail_container5").hide();
//     // player5.playVideo();
// });

// var player6;

// onYouTubeIframeAPIReady = function() {
//     player6 = new YT.Player('player6', {
//         height: '244',
//         width: '434',
//         videoId: 'AkyQgpqRyBY', // youtube video id
//         playerVars: {
//             'autoplay': 0,
//             'rel': 0,
//             'showinfo': 0
//         },
//         events: {
//             'onStateChange': onPlayerStateChange
//         }
//     });
// }

// var p = document.getElementById("player6");
// $(p).hide();

// var t = document.getElementById("thumbnail6");
// t.src = "img/content/video-preview.jpg";

// onPlayerStateChange = function(event) {
//     if (event.data == YT.PlayerState.ENDED) {
//         $('.play').fadeIn('normal');
//     }
// }

// $(document).on('click', '#play6', function() {
//     $(this).hide();
//     $("#player6").show();
//     $("#thumbnail_container6").hide();
//     // player.playVideo();
// });

// var player7;

// onYouTubeIframeAPIReady = function() {
//     player7 = new YT.Player('.player7', {
//         height: '244',
//         width: '434',
//         videoId: 'AkyQgpqRyBY', // youtube video id
//         playerVars: {
//             'autoplay': 0,
//             'rel': 0,
//             'showinfo': 0
//         },
//         events: {
//             'onStateChange': onPlayerStateChange
//         }
//     });
// }

// var p = document.getElementById("player7");
// $(p).hide();

// var t = document.getElementById("thumbnail7");
// t.src = "img/content/video-preview.jpg";

// onPlayerStateChange = function(event) {
//     if (event.data == YT.PlayerState.ENDED) {
//         $('.play').fadeIn('normal');
//     }
// }

// $(document).on('click', '#play7', function() {
//     $(this).hide();
//     $("#player7").show();
//     $("#thumbnail_container7").hide();
//     // player.playVideo();
// });
