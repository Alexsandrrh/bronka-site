$(document).ready(function($) {
  $(window).load(function() {
    setTimeout(function() {
      $("#preloader").fadeOut("slow", function() {
        // animatePlanet();

        AOS.init({
          once: true
        });

        var $spincrement = $(".js-spincrement");

        if ($spincrement.length) {
          for (var index = 0; index < $spincrement.length; index++) {
            var elem = $spincrement[index];

            spincrement(elem);
          }
        }

        $("body").removeClass("noscroll");
      });
    }, 10);
  });
});
