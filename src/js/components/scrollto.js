var timeout = null;

$(document).on('click', '.js-scrollto', function (){
    var elem = $(this);
    clearTimeout(timeout);

    timeout = setTimeout(function() {
        $('html, body').animate({
            scrollTop: $(elem.attr('href')).offset().top
        }, 1000);
    }, 300)
    return false;
});