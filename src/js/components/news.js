$(function() {
    fixedRubrics();

    var categoriesContainer = $('.js-section--newslist');
    var categoriesItems = categoriesContainer.find('.press-center_rubrics a');
    var categoriesDropdownMenu = categoriesContainer.find('.dropdown-menu');

    $(categoriesItems).each(function() {
        if($(this).parent().hasClass('current')) {
            $('.dropdown-toggle').html($(this).html());
        }

        var el = $(this).clone();
        el.addClass('dropdown-item');

        categoriesDropdownMenu.append(el);
    });

    $(window).on('scroll', function() {
        fixedRubrics();
    })
});

function fixedRubrics() {
    var $window = $(window);
    var $container = $('.press-center_rubrics-container');
    if(!$container.length) return false;
    var $rubrics = $container.find('.press-center_rubrics');

    var rubricsOuterHeight = $rubrics.outerHeight();
    var containerOffset = $container.offset();
    var windowHeight = $window.height();

    var scrollTop = ($window.scrollTop() + windowHeight) - rubricsOuterHeight;

    if(scrollTop > containerOffset.top) {
        $rubrics.removeClass('fixed')
    }else {
        $rubrics.addClass('fixed')
    }
}