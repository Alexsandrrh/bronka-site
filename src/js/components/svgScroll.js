var svgScroll = (function() {
  var svg = document.getElementById("footer-icon-mapline");
  var svg2 = document.getElementById("footer-icon-mapline2");
  var lines = document.getElementById("maplines");
  var footer = document.getElementById("footer");

  if (svg) {
    svgPath = document.querySelectorAll(".footer #footer-icon-mapline .group");
    svgPath2 = document.querySelectorAll(
      ".footer #footer-icon-mapline2 .group"
    );
    (svgPath3 = document.querySelectorAll(".footer #maplines .group-line")),
      (windowMargin = window.innerHeight / 8),
      (svgRect = svg.getBoundingClientRect()),
      (svgPos = svgRect.top - 490);
    (svgRect2 = svg2.getBoundingClientRect()), (svgPos2 = svgRect2.top - 510);
    (svgRect3 = lines.getBoundingClientRect()), (svgPos3 = svgRect3.top - 500);

    return {
      grow: function() {
        svgPath.forEach(function(item) {
          item.style.animation = "drawMapline 9s linear";
          item.style.animationFillMode = "forwards";
          footer.style.animation = "mapOpacity 3s linear";
          footer.style.animationFillMode = "forwards";
        });
      },
      grow2: function() {
        svgPath2.forEach(function(item) {
          item.style.animation = "drawMapline 7s linear";
          item.style.animationFillMode = "forwards";
        });
      },
      grow3: function() {
        svgPath3.forEach(function(item) {
          item.style.animation = "drawLines 4s linear";
          item.style.animationFillMode = "forwards";
        });
      }
    };
  }
})();

function mapsy() {
  var svgMap = document.getElementById("footer-icon-mapline");
  var svgMap2 = document.getElementById("footer-icon-mapline2");
  var svgMap3 = document.getElementById("maplines");

  if (svgMap && svgMap2 && svgMap3) {
    var screenHeight = $(window).height();

    var position = $("#footer").offset().top - screenHeight - 100,
      height =
        $("#footer").height() +
        Number(
          $("#footer")
            .css("padding-top")
            .replace("px", "")
        );

    var scroll = $(document).scrollTop();
    if (scroll > position && scroll < position + height) {
      console.log("target");
      svgScroll.grow();
      svgScroll.grow2();
      svgScroll.grow3();
    } else {
      console.log("no target");
    }
  }
}

$(document).ready(function($) {
  $(window).load(function() {
    setTimeout(mapsy, 2000);
  });
});

window.onscroll = mapsy;

window.addEventListener("resize", () => {
  mapsy();
});
