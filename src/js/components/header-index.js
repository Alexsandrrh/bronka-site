$(document).ready(function($) {
    var $headerIndex = $('.header--index');
    var $headerInner = $('.header--inner');
    var point = 400;
    var fixedClass = 'fixed';
    var showClass = 'show';

    var lastScrollTop = 0;

    $(window).scroll(function(event){
        if($('body').hasClass('menu-open')) return false;

        var st = $(this).scrollTop();

        if(st < point) {
            $headerInner.removeClass(fixedClass);
        }else {
            $headerInner.addClass(fixedClass);
        }

        if (st > lastScrollTop || st < point){
            $headerIndex.removeClass(showClass);
            $headerInner.removeClass(showClass);
        } else {
            $headerIndex.addClass(showClass);
            $headerInner.addClass(showClass);
        }

        lastScrollTop = st;
    });
});