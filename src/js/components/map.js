function initGoogleMap() {
  var mapBlock = document.getElementById("map");
  if (!mapBlock) return false;
  var myLatLng = {
      lat: Number(mapBlock.dataset.lat),
      lng: Number(mapBlock.dataset.lng)
    },
    markers;

  if (mapBlock.dataset.markers != "" && mapBlock.dataset.markers != undefined) {
    markers = JSON.parse(mapBlock.dataset.markers);
  }

  var map = new google.maps.Map(mapBlock, {
    zoom: 14,
    center: myLatLng,
    styles: [
      {
        featureType: "administrative",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#444444"
          }
        ]
      },
      {
        featureType: "landscape",
        elementType: "all",
        stylers: [
          {
            color: "#f2f2f2"
          }
        ]
      },
      {
        featureType: "poi",
        elementType: "all",
        stylers: [
          {
            visibility: "off"
          }
        ]
      },
      {
        featureType: "road",
        elementType: "all",
        stylers: [
          {
            saturation: -100
          },
          {
            lightness: 45
          }
        ]
      },
      {
        featureType: "road.highway",
        elementType: "all",
        stylers: [
          {
            visibility: "simplified"
          }
        ]
      },
      {
        featureType: "road.arterial",
        elementType: "labels.icon",
        stylers: [
          {
            visibility: "off"
          }
        ]
      },
      {
        featureType: "transit",
        elementType: "all",
        stylers: [
          {
            visibility: "off"
          }
        ]
      },
      {
        featureType: "water",
        elementType: "all",
        stylers: [
          {
            color: "#9fc8ed"
          },
          {
            visibility: "on"
          }
        ]
      }
    ]
  });

  var image = {
    url: "img/marker.png",
    size: new google.maps.Size(28, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(14, 38)
  };

  if (typeof markers == "object" && Object.keys(markers).length > 0) {
    // Область показа маркеров
    var markersBounds = new google.maps.LatLngBounds();

    for (var i = 0; i < markers.length; i++) {
      var markerPosition = new google.maps.LatLng(
        markers[i]["latitude"],
        markers[i]["longitude"]
      );

      // Добавляем координаты маркера в область
      markersBounds.extend(markerPosition);

      // Создаём маркер
      var marker = new google.maps.Marker({
        position: markerPosition,
        map: map,
        icon: image
      });
    }

    // Центрируем и масштабируем карту, если точек > 1
    if (markers.length > 1) {
      map.setCenter(markersBounds.getCenter(), map.fitBounds(markersBounds));
    }
  } else {
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: image
    });
  }
}

function InitMultiMap() {
  var container = $(".js-multimap");
  var mapBlock = document.getElementById(container.data("map"));
  var markers = [];
  var markerCluster;

  if (!container.length && !mapBlock) return false;

  var Popup = createPopupClass();

  var points = container.find(".js-multimap-point");

  var image = {
    url: "img/marker.png",
    size: new google.maps.Size(29, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(14, 38)
  };

  var imageCurrent = {
    url: "img/marker-current.png",
    size: new google.maps.Size(29, 38),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(14, 38)
  };

  var bounds = new google.maps.LatLngBounds();

  var map = new google.maps.Map(mapBlock, {
    zoom: 9,
    center: new google.maps.LatLng(59.929347, 30.336539),
    styles: [
      {
        featureType: "administrative",
        elementType: "labels.text.fill",
        stylers: [
          {
            color: "#444444"
          }
        ]
      },
      {
        featureType: "landscape",
        elementType: "all",
        stylers: [
          {
            color: "#f2f2f2"
          }
        ]
      },
      {
        featureType: "poi",
        elementType: "all",
        stylers: [
          {
            visibility: "off"
          }
        ]
      },
      {
        featureType: "road",
        elementType: "all",
        stylers: [
          {
            saturation: -100
          },
          {
            lightness: 45
          }
        ]
      },
      {
        featureType: "road.highway",
        elementType: "all",
        stylers: [
          {
            visibility: "simplified"
          }
        ]
      },
      {
        featureType: "road.arterial",
        elementType: "labels.icon",
        stylers: [
          {
            visibility: "off"
          }
        ]
      },
      {
        featureType: "transit",
        elementType: "all",
        stylers: [
          {
            visibility: "off"
          }
        ]
      },
      {
        featureType: "water",
        elementType: "all",
        stylers: [
          {
            color: "#9fc8ed"
          },
          {
            visibility: "on"
          }
        ]
      }
    ]
  });

  let changeVisibleDescriptions = function() {
    setTimeout(function() {
      if (!!markerCluster) {
        if (markerCluster.hasOwnProperty("clusters_")) {
          for (let clusterKey in markerCluster.clusters_) {
            if (markerCluster.clusters_.hasOwnProperty(clusterKey)) {
              let cluster = markerCluster.clusters_[clusterKey],
                arMarkers = cluster.getMarkers();

              if (arMarkers.length > 1) {
                for (let markerKey in arMarkers) {
                  if (arMarkers.hasOwnProperty(markerKey)) {
                    $("#content-map" + arMarkers[markerKey].index).hide();
                  }
                }
              } else {
                $("#content-map" + arMarkers[0].index).show();
              }
            }
          }
        }
      }
    }, 500);
  };

  if (points.length) {
    for (var i = 0; i < points.length; i++) {
      var point = $(points[i]);

      var marker = new google.maps.Marker({
        position: {
          lat: Number(point.data("lat")),
          lng: Number(point.data("lng"))
        },
        map: map,
        title: point.data("name"),
        icon: image,
        index: i
      });

      markers.push(marker);

      popup = new Popup(
        marker.position,
        document.getElementById("content-map" + i)
      );

      if (points.length === 1) {
        map.setCenter({
          lat: Number(point.data("lat")),
          lng: Number(point.data("lng"))
        });
      } else {
        map.setCenter({
          lat: 59.929347,
          lng: 30.336539
        });
      }

      popup.setMap(map);

      bounds.extend(marker.position);

      google.maps.event.addListener(
        marker,
        "click",
        (function(marker, i) {
          return function() {
            for (let i = 0; i < markers.length; i++) {
              markers[i].setIcon(image);
            }

            points
              .removeClass("current")
              .eq(marker.index)
              .addClass("current");

            marker.setIcon(imageCurrent);
          };
        })(marker, i)
      );

      google.maps.event.addListener(
        map,
        "bounds_changed",
        changeVisibleDescriptions
      );

      point.on("click", function() {
        let item = $(this);
        let index = item.index();
        let coords = {
          lat: item.data("lat"),
          lng: item.data("lng")
        };

        map.setCenter(coords);
        map.setZoom(17);

        points.removeClass("current");

        $(this).addClass("current");

        for (let j = 0; j < markers.length; j++) {
          if (index == j) {
            markers[j].setIcon(imageCurrent);
          } else {
            markers[j].setIcon(image);
          }
        }
      });
    }
  }

  var contentString = [
    '<div class="map-content">' +
      '<div id="illago">' +
      "</div>" +
      '<h1 class="map-title">Ресторан Il Lago Dei Cigni</h1>' +
      '<div class="map-links">' +
      '<a class="map-link" href="#">illago.ru</a>' +
      '<a class="map-link" href="#">instagram</a>' +
      '<a class="map-link" href="#">facebook</a>' +
      "</div>" +
      '<p class="map-address">Россия, г.Санкт-Петербург, Крестовский проспект, 21Б</p>' +
      "</div>",

    '<div class="map-content">' +
      '<div id="hunt">' +
      "</div>" +
      '<h1 class="map-title">Коктейльный бар "Архитектор"</h1>' +
      '<div class="map-links">' +
      '<a class="map-link" href="#">illago.ru</a>' +
      '<a class="map-link" href="#">instagram</a>' +
      '<a class="map-link" href="#">facebook</a>' +
      "</div>" +
      '<p class="map-address">191002, г.Санкт-Петербург, ул.Рубинштейна, 13</p>' +
      "</div>",

    '<div class="map-content">' +
      '<div id="farsh">' +
      "</div>" +
      '<h1 class="map-title">Ресторан"Фаршировка"</h1>' +
      '<div class="map-links">' +
      '<a class="map-link" href="#">illago.ru</a>' +
      '<a class="map-link" href="#">instagram</a>' +
      '<a class="map-link" href="#">facebook</a>' +
      "</div>" +
      '<p class="map-address">188662, Ленинградская область, Мурино, бульвар Менделеева, 16А</p>' +
      "</div>",

    '<div class="map-content">' +
      '<div id="buddha">' +
      "</div>" +
      '<h1 class="map-title">Ресторан "Buddha-Bar"</h1>' +
      '<div class="map-links">' +
      '<a class="map-link" href="#">illago.ru</a>' +
      '<a class="map-link" href="#">instagram</a>' +
      '<a class="map-link" href="#">facebook</a>' +
      "</div>" +
      '<p class="map-address">191124, г.Санкт-Петербург, Синопская набережная, 78</p>' +
      "</div>",

    '<div class="map-content">' +
      '<div id="lunka">' +
      "</div>" +
      '<h1 class="map-title">Ресторан "Лунка19"</h1>' +
      '<div class="map-links">' +
      '<a class="map-link" href="#">illago.ru</a>' +
      '<a class="map-link" href="#">instagram</a>' +
      '<a class="map-link" href="#">facebook</a>' +
      "</div>" +
      '<p class="map-address">198510, г.Санкт-Петербург, Петергоф, ул.Гофмейстерская, 1</p>' +
      "</div>"
  ];

  for (let i = 0; i < markers.length; i++) {
    markers[i].addListener("click", function() {
      var infowindow = new google.maps.InfoWindow({
        content: contentString[i],
        maxWidth: 400
      });
      if (infowindow) {
        infowindow.close();
      }
      infowindow.setPosition(event.latLng);
      infowindow.open(map, markers[i]);
    });
  }

  var markerCluster = new MarkerClusterer(map, markers, {
    imagePath:
      "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m"
  });

  // if (points.length > 1) {
  //     map.fitBounds(bounds);

  //     MarkerClusterer.prototype.createClusters_ = function() {
  //         if (!this.ready_) {
  //             return;
  //         }

  //         // Get our current map view bounds.
  //         // Create a new bounds object so we don't affect the map.
  //         var mapBounds = new google.maps.LatLngBounds(this.map_.getBounds().getSouthWest(), this.map_.getBounds().getNorthEast());
  //         var bounds = this.getExtendedBounds(mapBounds);

  //         for (var i = 0, marker; marker = this.markers_[i]; i++) {
  //             if (!marker.isAdded && this.isMarkerInBounds_(marker, bounds)) {
  //                 this.addToClosestCluster_(marker);
  //             }
  //         }

  //         google.maps.event.trigger(this, "clusteringend", this);
  //     };

  //     markerCluster = new MarkerClusterer(map, markers, {
  //         imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
  //     });

  //     google.maps.event.addListener(markerCluster, 'clusteringend', changeVisibleDescriptions);
  // } else {}
}

function createPopupClass() {
  /**
   * A customized popup on the map.
   * @param {!google.maps.LatLng} position
   * @param {!Element} content The bubble div.
   * @constructor
   * @extends {google.maps.OverlayView}
   */
  function Popup(position, content) {
    this.position = position;

    content.classList.add("popup-bubble");

    var bubbleAnchor = document.createElement("div");
    bubbleAnchor.classList.add("popup-bubble-anchor");
    bubbleAnchor.appendChild(content);

    this.containerDiv = document.createElement("div");
    this.containerDiv.classList.add("popup-container");
    this.containerDiv.appendChild(bubbleAnchor);

    google.maps.OverlayView.preventMapHitsAndGesturesFrom(this.containerDiv);
  }

  Popup.prototype = Object.create(google.maps.OverlayView.prototype);

  Popup.prototype.onAdd = function() {
    this.getPanes().floatPane.appendChild(this.containerDiv);
  };

  Popup.prototype.onRemove = function() {
    if (this.containerDiv.parentElement) {
      this.containerDiv.parentElement.removeChild(this.containerDiv);
    }
  };

  Popup.prototype.draw = function() {
    var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);

    var display =
      Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000
        ? "block"
        : "none";

    if (display === "block") {
      this.containerDiv.style.left = divPosition.x + "px";
      this.containerDiv.style.top = divPosition.y + "px";
    }
    if (this.containerDiv.style.display !== display) {
      this.containerDiv.style.display = display;
    }
  };

  return Popup;
}
