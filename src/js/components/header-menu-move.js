const header = document.querySelector('.header_menu');
var widthScreen = screen.width;

if (header) {
    window.addEventListener('DOMContentLoaded', function() {
        if (widthScreen < 480) {
            function moveMenuCompany() {
                const inner = document.querySelector('.inner-list');
                const linkCompany = document.querySelector('.link--company');
                const backCompany = document.getElementById('company-back');

                linkCompany.addEventListener('click', e => {
                    e.preventDefault();
                    console.log('clickCompany');
                    inner.classList.add('inner-list--active');
                })

                backCompany.addEventListener('click', e => {
                    e.preventDefault();
                    inner.classList.remove('inner-list--active');
                })
            };

            moveMenuCompany();
        }


    });

    window.addEventListener('DOMContentLoaded', function() {
        if (widthScreen < 480) {
            function moveMenuBuisness() {
                const inner = document.querySelector('.inner-list');
                const linkBuisness = document.querySelector('.link--buisness');
                const backBuisness = document.getElementById('buisness-back');


                linkBuisness.addEventListener('click', e => {
                    e.preventDefault();
                    console.log('clickBuisness');
                    inner.classList.add('inner-list--active');
                })

                backBuisness.addEventListener('click', e => {
                    e.preventDefault();
                    inner.classList.remove('inner-list--active');
                })
            };

            moveMenuBuisness();
        }


    });

    window.addEventListener('DOMContentLoaded', function() {
        if (widthScreen < 480) {
            function moveMenuPress() {
                const inner = document.querySelector('.inner-list');
                const linkPress = document.querySelector('.link--press');
                const backPress = document.getElementById('press-back');

                linkPress.addEventListener('click', e => {
                    e.preventDefault();
                    console.log('clickPress');
                    inner.classList.add('inner-list--active');
                })

                backPress.addEventListener('click', e => {
                    e.preventDefault();
                    inner.classList.remove('inner-list--active');
                })
            };

            moveMenuPress();
        }

    });
};